package com.somaxa8.erp.productcatalog.controller

import com.somaxa8.erp.productcatalog.domain.Category
import com.somaxa8.erp.productcatalog.service.CategoryService
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.web.bind.annotation.*

@RestController
class CategoryController(
    private val categoryService: CategoryService,
) {

    @GetMapping("/categories")
    fun getAll(
        @RequestParam(value = "page", defaultValue = "0") page: Int,
        @RequestParam(value = "size", defaultValue = "10") size: Int,
        @ModelAttribute category: Category,
    ): Page<Category> {
        return categoryService.findAll(category, PageRequest.of(page, size))
    }

    @GetMapping("/categories/{id}")
    fun get(@PathVariable id: Long): Category {
        return categoryService.findById(id)
    }

    @PutMapping("/categories/{id}")
    fun update(@PathVariable id: Long, @RequestBody category: Category): Category {
        return categoryService.update(id, category)
    }

    @PostMapping("/categories")
    fun create(@RequestBody category: Category): Category {
        return categoryService.create(category)
    }

    @DeleteMapping("/categories/{id}")
    fun delete(@PathVariable id: Long) {
        categoryService.deleteById(id)
    }
}