package com.somaxa8.erp.productcatalog.controller

import com.somaxa8.erp.productcatalog.domain.Product
import com.somaxa8.erp.productcatalog.service.ProductService
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.web.bind.annotation.*

@RestController
class ProductController(
    private val _productService: ProductService,
) {

    @GetMapping("/products")
    fun getAll(
        @RequestParam(value = "page", defaultValue = "0") page: Int,
        @RequestParam(value = "size", defaultValue = "10") size: Int,
        @ModelAttribute product: Product,
    ): Page<Product> {
        return _productService.findAll(product, PageRequest.of(page, size))
    }

    @GetMapping("/products/{id}")
    fun get(@PathVariable id: Long): Product {
        return _productService.findById(id)
    }

    @PutMapping("/products/{id}")
    fun update(@PathVariable id: Long, @RequestBody product: Product): Product {
        return _productService.update(id, product)
    }

    @PostMapping("/products")
    fun create(@RequestBody product: Product): Product {
        return _productService.create(product)
    }

    @DeleteMapping("/products/{id}")
    fun delete(@PathVariable id: Long) {
        _productService.deleteById(id)
    }
}