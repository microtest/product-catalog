package com.somaxa8.erp.productcatalog.domain

import com.somaxa8.erp.productcatalog.utils.Identifiable
import jakarta.persistence.Entity
import jakarta.persistence.GeneratedValue
import jakarta.persistence.Id
import java.io.Serializable

@Entity
data class Category(
    @Id
    @GeneratedValue
    override var id: Long? = null,

    var name: String? = null,

    var description: String? = null,
): Identifiable<Long>, Serializable