package com.somaxa8.erp.productcatalog.domain

import com.fasterxml.jackson.annotation.JsonIgnore
import com.somaxa8.erp.productcatalog.utils.Identifiable
import jakarta.persistence.*
import java.io.Serializable

@Entity
data class Product(
    @Id
    @GeneratedValue
    override var id: Long? = null,

    @Column(nullable = false)
    var name: String? = null,

    @Column(nullable = false)
    var description: String? = null,

    @Column(nullable = false)
    var price: Float? = null,

    @Column(nullable = false)
    var quantityInStock: Int? = null,

    @Column(name = "product_category_id", nullable = false)
    var categoryId: Long? = null,

    var enabled: Boolean? = null,

): Identifiable<Long>, Serializable {

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "product_category_id", insertable = false, nullable = false, updatable = false)
    var category: Category? = null
}
