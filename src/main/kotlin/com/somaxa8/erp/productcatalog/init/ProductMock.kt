package com.somaxa8.erp.productcatalog.init

import com.github.javafaker.Faker
import com.somaxa8.erp.productcatalog.domain.Product
import com.somaxa8.erp.productcatalog.service.ProductService
import jakarta.annotation.PostConstruct
import org.springframework.stereotype.Component
import kotlin.random.Random

@Component
class ProductMock(
    private val _productService: ProductService,
) {

    @PostConstruct
    fun init() {
//        if (_productService.count() != 0L) {
            return
//        }

        val faker = Faker()

        val products = (1..1000).map {
            Product(
                name = faker.commerce().productName(),
                description = faker.lorem().sentence(10),
                price = Random.nextDouble(1.0, 1000.0).toFloat(),
                quantityInStock = Random.nextInt(10, 10000),
            )
        }

        _productService.createAll(products)
    }
}