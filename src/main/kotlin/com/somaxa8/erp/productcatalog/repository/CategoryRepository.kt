package com.somaxa8.erp.productcatalog.repository

import com.somaxa8.erp.productcatalog.domain.Category
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface CategoryRepository : JpaRepository<Category, Long> {
}