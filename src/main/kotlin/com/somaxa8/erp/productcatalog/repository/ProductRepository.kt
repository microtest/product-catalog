package com.somaxa8.erp.productcatalog.repository

import com.somaxa8.erp.productcatalog.domain.Product
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface ProductRepository : JpaRepository<Product, Long> {
}