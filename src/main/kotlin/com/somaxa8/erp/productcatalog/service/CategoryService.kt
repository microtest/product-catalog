package com.somaxa8.erp.productcatalog.service

import com.somaxa8.erp.productcatalog.domain.Category
import com.somaxa8.erp.productcatalog.repository.CategoryRepository
import com.somaxa8.erp.productcatalog.utils.BaseService
import org.springframework.stereotype.Service

@Service
class CategoryService(
    private val categoryRepository: CategoryRepository,
): BaseService<Category, Long>(categoryRepository) {
}