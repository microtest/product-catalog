package com.somaxa8.erp.productcatalog.service

import com.somaxa8.erp.productcatalog.domain.Product
import com.somaxa8.erp.productcatalog.repository.ProductRepository
import com.somaxa8.erp.productcatalog.utils.BaseService
import org.springframework.stereotype.Service

@Service
class ProductService(
    private val _productRepository: ProductRepository,
) : BaseService<Product, Long>(_productRepository) {

    fun count(): Long {
        return _productRepository.count()
    }
}