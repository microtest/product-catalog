package com.somaxa8.erp.productcatalog.utils

interface Identifiable<T> {
    var id: T?
}